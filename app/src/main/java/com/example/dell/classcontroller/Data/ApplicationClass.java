package com.example.dell.classcontroller.Data;

import android.app.Application;

import com.example.dell.classcontroller.UI.FontClass;


public class ApplicationClass extends Application {
    @Override
    public void onCreate(){
        super.onCreate();

        FontClass.setDefaultFont(this, "DEFAULT", "HGGGothicssi_20g.ttf");
        FontClass.setDefaultFont(this, "MONOSPACE", "HGGGothicssi_20g.ttf");
        FontClass.setDefaultFont(this, "SERIF", "HGGGothicssi_20g.ttf");
        FontClass.setDefaultFont(this, "SANS_SERIF", "HGGGothicssi_20g.ttf");
    }
}
