package com.example.dell.classcontroller.Data;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.dell.classcontroller.Exception.EmptyDataFromServerException;
import com.loopj.android.http.*;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

public class DataLoader {
    private static final String TAG = "DataLoaderDebug";
    //서버로부터 Data를 받아오는 기능 담당. get.
    private static DataLoader userData;
    private static JSONObject tempResponse;
    private Context context;
    public ClassData class101, class102;

    public static synchronized DataLoader getInstance() {
        if (userData == null) {
            userData = new DataLoader();
        }
        return userData;

    }

    private DataLoader() {
        class101 = new ClassData(101);
        class102 = new ClassData(102);

    }

    public void loadRoomData(int classNum, JsonHttpResponseHandler jsonHttpResponseHandler) throws JSONException {
        //classNum으로 데이터를 받아온다.

        final int rawClassNum = classNum;

        if (classNum % 2 == 0) {
            classNum = 2;
        } else classNum = 1;

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://growingdever.cafe24.com:3001/" + classNum, jsonHttpResponseHandler);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void checkValueOfJSON(JSONObject response) throws EmptyDataFromServerException, JSONException {
        if (response == null) {
            throw new EmptyDataFromServerException("null");
        } else if (!response.has("temp") || response.isNull("temp")) {
            throw new EmptyDataFromServerException("temp");
        } else if (!response.has("hum") || response.isNull("hum")) {
            throw new EmptyDataFromServerException("hum");
        } else if (!response.has("inuse") || response.isNull("inuse") || response.getString("inuse").contains("0")) {
            throw new EmptyDataFromServerException("inuse");
        } else if (!response.has("lum") || response.isNull("lum") || response.getString("lum").contains("0")) {
            throw new EmptyDataFromServerException("lum");
        } else if (!response.has("proj") || response.isNull("proj") || response.getString("proj").contains("0")) {
            throw new EmptyDataFromServerException("proj");
        } else if (!response.has("aircon") || response.isNull("aircon") || response.getString("aircon").contains("0")) {
            throw new EmptyDataFromServerException("aircon");
        }

    }
}
