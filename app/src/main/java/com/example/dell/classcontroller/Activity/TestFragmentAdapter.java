package com.example.dell.classcontroller.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dell.classcontroller.R;
import com.example.dell.classcontroller.UI.CircleProgressBar;

public class TestFragmentAdapter extends FragmentStatePagerAdapter {
    protected static final String[] CONTENT = new String[]{"temp", "hum", "person",};
    private static final int DEFAULT_SET = -200000;
    private int mCount = CONTENT.length;

    private int temp, hum = -1;
    private String inuse = "O";

    public TestFragmentAdapter(FragmentManager fm, int temp, int hum, String inuse) {
        super(fm);

        this.temp = temp;
        this.hum = hum;
        this.inuse = inuse;

    }

    @Override
    public Fragment getItem(int position) {
        //해당하는 page의 fragment를 생성한다
        //각 fragment 생성시 알맞은 값을 던져준다.
        switch (position) {
            case 0:
                return new TempFragment(temp);
            case 1:
                return new HumFragment(hum);
            case 2:
                return new InUseFragment(inuse);

        }
        return new TempFragment(27);

    }

    @Override
    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemPosition(Object item) {
        return POSITION_NONE;
    }

//
//    @Override
//    public void destroyItem(View pager, int position, Object view) {
//        ((ViewPager)pager).removeView((View)view);
//    }

    public static class TempFragment extends Fragment {
        int temper = 0;

        public TempFragment() {

        }

        public TempFragment(int temp) {
            this.temper = temp;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.page_temp, container, false);
            TextView textView = (TextView) view.findViewById(R.id.pager_text_temp);
            String tempStr = Integer.toString(temper) + "°C";
            tempStr = (temper == DEFAULT_SET) ? "N/A" : tempStr;
            textView.setText(tempStr);

            CircleProgressBar circleProgressBar = (CircleProgressBar) view.findViewById(R.id.custom_progressBar_temp);
//            circleProgressBar.setProgress(temper);
            circleProgressBar.setProgressWithAnimation(temper);
//            circleProgressBar.onDraw();


            return view;
        }

    }

    public static class HumFragment extends Fragment {
        int hum = 0;

        public HumFragment() {

        }

        public HumFragment(int hum) {
            this.hum = hum;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            //return inflater.inflate(R.layout.velocity, container, false);

            View view = inflater.inflate(R.layout.page_hum, container, false);
            TextView textView = (TextView) view.findViewById(R.id.pager_text_hum);
            String humStr = Integer.toString(hum) + "%";
            humStr = (hum == DEFAULT_SET) ? "N/A" : humStr;
            textView.setText(humStr);
            CircleProgressBar circleProgressBar = (CircleProgressBar) view.findViewById(R.id.custom_progressBar_hum);
//            circleProgressBar.setProgress(hum);
            circleProgressBar.setProgressWithAnimation(hum);
//            circleProgressBar.onDraw();
            return view;
        }

    }

    public static class InUseFragment extends Fragment {
        Boolean inuse = false;
        String inuseString = "OFF";

        public InUseFragment() {

        }

        public InUseFragment(String inuse) {
            if (inuse.contains("X")) {
                this.inuse = false;
                this.inuseString = "OFF";
            } else if (inuse.contains("O")) {
                this.inuse = true;
                this.inuseString = "ON";
            } else {
                //inuse error
                this.inuse = false;
                this.inuseString = "N/A";
            }
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // return inflater.inflate(R.layout.temperature, container, false);
            View view = inflater.inflate(R.layout.page_inuse, container, false);
            TextView textView = (TextView) view.findViewById(R.id.pager_text_inuse);

            textView.setText(inuseString);
            CircleProgressBar circleProgressBar = (CircleProgressBar) view.findViewById(R.id.custom_progressBar_inuse);
            if (inuse) {
//                circleProgressBar.setProgress(1);
                circleProgressBar.setProgressWithAnimation(1);
            } else {
//                circleProgressBar.setProgress(0);
                circleProgressBar.setProgressWithAnimation(0);
            }
//            circleProgressBar.onDraw();
            return view;
        }//end oncreate


    }//end default fragment
}
