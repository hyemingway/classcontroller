package com.example.dell.classcontroller.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.classcontroller.Data.ClassData;
import com.example.dell.classcontroller.Data.DataLoader;
import com.example.dell.classcontroller.Exception.EmptyDataFromServerException;
import com.example.dell.classcontroller.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.viewpagerindicator.CirclePageIndicator;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;


public class ControlActivity extends ActionBarActivity implements View.OnClickListener, View.OnTouchListener {
    private static final String TAG = "ControlActivityDebug";
    //default set value before response reach.
    private static final int DEFAULT_SET = -200000;
    private static final String DEFAULT_SET_STR = "N/A";

    private Context context;
    private ViewPager mViewPager;

    private Button lightButton;
    private Button beamButton;
    private Button airButton;

    private StringEntity entity;
    private DataLoader dataLoader;

    private int rawclassNum = 0;
    private String classNumString = "101";

    private int temp, hum = 0;
    private String inuse, lum, aircon, proj = null;

    private JsonHttpResponseHandler jsonHttpResponseHandler;
//    public ClassData class101, class102;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        this.context = ControlActivity.this;
        this.dataLoader = DataLoader.getInstance();


        //Handler for setting response value to UI.
        jsonHttpResponseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    int id = response.getInt("id");
                    int temp = response.getInt("temp");
                    int hum = response.getInt("hum");
                    String inuse = response.getString("inuse");
                    String light = response.getString("lum");
                    String proj = response.getString("proj");
                    String aircon = response.getString("aircon");

                    if (rawclassNum % 2 == 0) {
                        dataLoader.class102.setClassData(temp, hum, inuse, light, proj, aircon);
                    } else {
                        dataLoader.class101.setClassData(temp, hum, inuse, light, proj, aircon);
                    }

                    String result = "classNum" + rawclassNum + "&" + id + "|" +
                            "temp " + temp + "|" +
                            "hum " + hum + "|" +
                            "inuse " + inuse + "|" +
                            "light " + light + "|" +
                            "aircon " + aircon + "|" +
                            "proj " + proj + "|";

                    Log.d(TAG + statusCode, result);
                    Toast.makeText(context, "상태 확인 완료", Toast.LENGTH_LONG).show();

                    //Button Setting


                    setUpdateValue(temp, hum, inuse, light, proj, aircon);
                    if (lum.contains("O")) {
                        lightButton.setPressed(true);
                        Toast.makeText(context, "lum on", Toast.LENGTH_LONG).show();
                    } else {
                        lightButton.setPressed(false);
                    }
                    if (proj.contains("O")) {
                        beamButton.setPressed(true);
                    } else {
                        beamButton.setPressed(false);
                    }
                    if (aircon.contains("O")) {
                        airButton.setPressed(true);
                    } else {
                        airButton.setPressed(false);
                    }
//
//                    if (rawclassNum % 2 == 0) {
//                        temp = dataLoader.class102.getCurrentTemp();
//                        hum = dataLoader.class102.getCurrentHum();
//                        inuse = dataLoader.class102.getCurrentInuse();
//                        lum = dataLoader.class102.getCurrentLum();
//                        proj = dataLoader.class102.getCurrentProj();
//                        aircon = dataLoader.class102.getCurrentAircon();
//                        Log.d(TAG, "102 data get" + temp);
//
//                    } else {
//                        temp = dataLoader.class101.getCurrentTemp();
//                        hum = dataLoader.class101.getCurrentHum();
//                        inuse = dataLoader.class101.getCurrentInuse();
//                        lum = dataLoader.class101.getCurrentLum();
//                        proj = dataLoader.class101.getCurrentProj();
//                        aircon = dataLoader.class101.getCurrentAircon();
//                        Log.d(TAG, "101 data get" + temp);
//                    }

                    Log.d(TAG, "다시 set Layout");
                    setLayout();
                    Log.d(TAG, "다시 set Layout");


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, e.getMessage());
                    Toast.makeText(context, "상태 확인 실패", Toast.LENGTH_LONG).show();

                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {

                if (response == null) {
                    Toast.makeText(context, "response null 서버가 꺼져있습니다.", Toast.LENGTH_LONG).show();


                    //여기로 들어옴. 보통..
                } else {
                    Toast.makeText(context, "fail 서버가 꺼져있습니다.", Toast.LENGTH_SHORT).show();
                }
                lightButton.setClickable(false);
                beamButton.setClickable(false);
                airButton.setClickable(false);

            }
        };


        //classNumber set
        Intent intent = getIntent();
        classNumString = intent.getStringExtra("classNum");
        rawclassNum = Integer.parseInt(classNumString);

        //action bar set
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        TextView title = (TextView) findViewById(R.id.actionbar_title_custom);

        //title set
        title.setText(classNumString);
        //UI DEFAULT set
        lightButton = (Button) findViewById(R.id.light_button);
        beamButton = (Button) findViewById(R.id.beam_button);
        airButton = (Button) findViewById(R.id.air_button);
        //button set listener
        lightButton.setOnClickListener(this);
        beamButton.setOnClickListener(this);
        airButton.setOnClickListener(this);

        //set DEFAULT UI
        setLayout();
        try {
            setAsyncClient();   //set response value to UI
        } catch (EmptyDataFromServerException e) {
            e.printStackTrace();
            Toast.makeText(context, "서버에서 값을 받아오지 못했습니다.", Toast.LENGTH_LONG).show();
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void setUpdateValue(int temp, int hum, String inuse, String light, String proj, String aircon) {
        this.temp = temp;
        this.hum = hum;
        this.inuse = inuse;
        this.lum = light;
        this.proj = proj;
        this.aircon = aircon;
    }

    private void setLayout() {

        if(lum != null && proj != null && aircon != null){
            if (lum.contains("O")){
                lightButton.setPressed(true);
            }else{
                lightButton.setPressed(false);
            }
            if(proj.contains("O")){
                beamButton.setPressed(true);
            }else{
                beamButton.setPressed(false);
            }
            if(aircon.contains("O")){
                airButton.setPressed(true);
            }else{
                airButton.setPressed(false);
            }

        }else{
            Log.d(TAG, "버튼 변수가 null");
        }
        if(inuse == null){
            temp = DEFAULT_SET;
            hum = DEFAULT_SET;
            inuse = DEFAULT_SET_STR;
            Log.d(TAG, "inuse 변수가 null");

        }

        //viewpager set
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        TestFragmentAdapter testFragmentAdapter = new TestFragmentAdapter(getSupportFragmentManager(), temp, hum, inuse);
        testFragmentAdapter.notifyDataSetChanged();
        mViewPager.setAdapter(testFragmentAdapter);
        CirclePageIndicator dotIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        dotIndicator.setViewPager(mViewPager);

        mViewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                page.setTranslationX(page.getWidth() * -position);
                if (position <= -1.0F || position >= 1.0F) {
                    page.setAlpha(0.0F);
                } else if (position == 0.0F) {
                    page.setAlpha(1.0F);
                } else {
                    // position is between -1.0F & 0.0F OR 0.0F & 1.0F
                    page.setAlpha(1.0F - Math.abs(position));
                }
            }
        });

    }

    private void setAsyncClient() throws EmptyDataFromServerException, JSONException {
        //AsyncHttpClient set
        dataLoader.setContext(this);
        dataLoader.loadRoomData(rawclassNum, jsonHttpResponseHandler);


    }




    @Override
    public void onClick(View view) {
//
//        if (this.rawclassNum % 2 == 0) {
//            this.temp = dataLoader.class102.getCurrentTemp();
//            this.hum = dataLoader.class102.getCurrentHum();
//            this.inuse = dataLoader.class102.getCurrentInuse();
//            this.lum = dataLoader.class102.getCurrentLum();
//            this.proj = dataLoader.class102.getCurrentProj();
//            this.aircon = dataLoader.class102.getCurrentAircon();
//            Log.d(TAG, "102 data get");
//
//        } else {
//            this.temp = dataLoader.class101.getCurrentTemp();
//            this.hum = dataLoader.class101.getCurrentHum();
//            this.inuse = dataLoader.class101.getCurrentInuse();
//            this.lum = dataLoader.class101.getCurrentLum();
//            this.proj = dataLoader.class101.getCurrentProj();
//            this.aircon = dataLoader.class101.getCurrentAircon();
//            Log.d(TAG, "101 data get");
//        }

        Button b = (Button) view;
        int classNum;
        if (rawclassNum % 2 == 0) classNum = 102;
        else classNum = 101;

        String name = b.getText().toString();

        if (b.isPressed() || b.isSelected()) {

        } else {

        }

        switch (name) {
            case "Light":
                if (lum.contains("O")) {
                    lum = "X";
                    b.setPressed(false);
                    b.setSelected(false);
                } else {
                    lum = "O";
                    b.setPressed(true);
                    b.setSelected(true);
                }
                break;
            case "Projector":
                if (proj.contains("O")) {
                    proj = "X";
                    b.setPressed(false);
                    b.setSelected(false);
                } else {
                    proj = "O";
                    b.setPressed(true);
                    b.setSelected(true);

                }
                break;
            case "Air Conditioner":
                if (aircon.contains("O")) {
                    aircon = "X";
                    b.setPressed(false);
                    b.setSelected(false);

                } else {
                    aircon = "O";
                    b.setPressed(true);
                    b.setSelected(true);

                }
                break;
        }


        JSONObject param;
        param = generateJSONParam(classNum, lum, proj, aircon);
        Log.d(TAG, "make param" + param.toString());

        try {
            entity = new StringEntity(param.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/json");
        client.post(getApplicationContext(), "http://growingdever.cafe24.com:3001/", entity, "application/json", new JsonHttpResponseHandler() {
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                //TODO 여기로 코드가 안들어감!
                Toast.makeText(context, "Post Success", Toast.LENGTH_LONG).show();
                Log.d(TAG, statusCode + "");
                Log.d(TAG, "SUCCESS");
                Log.d(TAG, response.toString());
                if (!response.toString().contains("success")) {
                    Toast.makeText(context, "이미 보낸 요청입니다.", Toast.LENGTH_SHORT).show();
                }

            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                if (response == null) {
                    Toast.makeText(context, "컨트롤러가 연결되지 않았습니다." + statusCode, Toast.LENGTH_LONG).show();
                    //TODO 성공했는데 여기 들어옴. 라즈베리 파이에서 갱신되지 않아서 그런듯...?
                } else {
                    Toast.makeText(context, "서버가 꺼져있습니다." + statusCode, Toast.LENGTH_LONG).show();
                }
            }
        });


//        try {
//            dataLoader.loadRoomData(rawclassNum, jsonHttpResponseHandler);
//        } catch (JSONException | EmptyDataFromServerException e) {
//            e.printStackTrace();
//        }

    }

    JSONObject generateJSONParam(int classNum, String lum, String proj, String aircon) {
        JSONObject param = new JSONObject();

        try {
            param.put("id", classNum);
            param.put("lum", lum);
            param.put("proj", proj);
            param.put("aircon", aircon);
            //조절하는 값이 아닌 경우는 DEFAULT_SET_STR. 한번에 한개만 컨트롤 가능하니까...
            param.put("control", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        Button b = (Button) view;
        // show interest in events resulting from ACTION_DOWN
        if (event.getAction() == MotionEvent.ACTION_DOWN) return true;
        // don't handle event unless its ACTION_UP so "doSomething()" only runs once.
        if (event.getAction() != MotionEvent.ACTION_UP) return false;
        b.setPressed(true);

        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
//                Toast.makeText(context, "Back", Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
        return true;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) { // 백 버튼
//            Toast.makeText(this, "Back", Toast.LENGTH_SHORT).show();
            finish();

        }
        return true;
    }


}
