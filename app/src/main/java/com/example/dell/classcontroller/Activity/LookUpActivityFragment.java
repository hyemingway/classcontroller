package com.example.dell.classcontroller.Activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.dell.classcontroller.Activity.ControlActivity;
import com.example.dell.classcontroller.R;


/**
 * A placeholder fragment containing a simple view.
 */
public class LookUpActivityFragment extends Fragment implements View.OnClickListener{

    private static final String TAG = "LookUpActivityFragment";
    private static final int BUTTON_NUM = 30;
    //TODO Button Num 숫자 변경
    public LookUpActivityFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_look_up, container, false);

        this.setLayout(v);

        return v;


    }


    private void setLayout(View view){
        Button[] classBtn = new Button[BUTTON_NUM];
        int startNum = R.id.btn_class101;
        for(int i = 0; i < BUTTON_NUM; i++){
            classBtn[i] = (Button)view.findViewById(startNum);
            classBtn[i].setTag(i);
            classBtn[i].setOnClickListener(this);
            startNum++;
        }

    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "clicked");
        Button b = (Button)view;
        String buttonText = b.getText().toString();

        int classNumber = Integer.parseInt(buttonText);
        if(classNumber < 0){
            Log.e(TAG, "class Number Exception");
        }

        int btnStartNum = R.id.btn_class101;
        Log.d(TAG, buttonText + " clicked");
//        Toast.makeText(this.getActivity()," " + buttonText, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this.getActivity(), ControlActivity.class);
        intent.putExtra("classNum", buttonText);
        startActivity(intent);

//        switch(view.getId()){
//            case R.id.btn_class101:
//                break;
//            case R.id.btn_class102:
//                break;
//        }
    }
}
