package com.example.dell.classcontroller.Exception;

/**
 * Created by Hyemingway on 15. 9. 19..
 */
public class EmptyDataFromServerException extends Exception{
    private String key;

    public EmptyDataFromServerException(String key) {
        this.key = key;
    }

    public String getKey(){
        return this.key;
    }
    @Override
    public String getMessage() {
        return " 서버에서 이 값 ("+ key +")을 받아오지 못했습니다.";
    }
}
