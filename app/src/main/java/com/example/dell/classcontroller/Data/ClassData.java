package com.example.dell.classcontroller.Data;

public class ClassData {
    private static int DEFAULT_SET= 0;
    private int id = 101;
    private int currentTemp, currentHum = DEFAULT_SET;
    private String currentLum, currentProj, currentAircon, currentInuse = null;
//    LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    View view = inflater.inflate(R.layout.mylayout, null);
//
//    Layout item = view.findViewById(R.id.desktopsFramelayout);


    public ClassData(int id){
        if(id % 2 == 0) this.id = 102;
        else this.id = 101;
    }

    public void setClassData(int temp, int hum, String inuse, String lum, String proj, String aircon) {

        this.currentTemp = temp;
        this.currentHum = hum;
        this.currentInuse = inuse;
        this.currentLum = lum;
        this.currentProj = proj;
        this.currentAircon = aircon;
    }

    public int getCurrentTemp() {
        return currentTemp;
    }
    public int getCurrentHum(){
        return currentHum;
    }

    public String getCurrentInuse() {
        return currentInuse;
    }

    public String getCurrentLum() {
        return currentLum;
    }
    public String getCurrentProj(){
        return currentProj;
    }
    public String getCurrentAircon(){
        return currentAircon;
    }

    public String toString(){
        String res = "id " + id + "::" + "temp " + currentTemp + "|" +
                    "hum " + currentHum + "|" +
                    "inuse " + currentInuse + "|" +
                    "light " + currentLum + "|" +
                    "proj " + currentProj + "|" +
                    "aircon " + currentAircon + "\n";

        return res;
    }
}
