package com.example.dell.classcontroller.Activity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.dell.classcontroller.R;


public class MainActivity extends ActionBarActivity {
    private static final String TAG = "MainActivity";



    private Button lookupButton;
    private Button controlButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        getSupportActionBar().hide();


        lookupButton = (Button)findViewById(R.id.buttonlookup);

        lookupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toLookup = new Intent(MainActivity.this, LookUpActivity.class);
                startActivity(toLookup);
                finish();
            }
        });


    }

}
